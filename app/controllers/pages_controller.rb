class PagesController < ApplicationController
  before_filter :subdomain_view_path

  def home

  end

  private

  def subdomain_view_path
    prepend_view_path "app/views/#{request.subdomain}" if request.subdomain.present?
  end
end
