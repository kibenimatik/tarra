Rails.application.routes.draw do
  get '/contact' => 'pages#contact', as: :contact_page
  get '/about' => 'pages#about', as: :about_page
  root to: 'pages#home'
end
